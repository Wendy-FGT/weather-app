const path = require('path')
const express = require('express')


const app = express()
const publicDirPath = path.join(__dirname, '../public/')

app.use(express.static(publicDirPath))

// app.get('', (req, res)=>{
//     res.send('<h1>Hello express!</h1>')
// })

// app.get('/help', (req, res) => {
//     res.send('Help page.')
// })
// //serving html
// app.get('/about', (req, res) => {
//     res.send('<h1>Finding God in daily ordinary life.<h1/>')
// })
// //serving json
app.get('/weather-info', (req,res) => {
    res.send({
        "country" :"South Korea",
        "temperature": '16 degrees'
    })
})
//app.com
//app.com/help
//app.com/about

app.listen(3000, () =>{
    console.log('Server started on port 3000.')
})