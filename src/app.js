const path = require('path')
const express = require('express')
const hbs = require('hbs')
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')

const app = express()
const port = process.env.PORT || 3000

//DEFINE PATHS FOR EXPRESS CONFIG
const publicDirPath = path.join(__dirname, '../public')
//customize views engine/ directory
const viewPath = path.join(__dirname, '../templates/views')
const viewPathPartials = path.join(__dirname, '../templates/partials')
//SETUP HANDLEBARS ENGINE AND VIEWS LOCATION
app.set('view engine', 'hbs')
//set customized view directory
app.set('views',viewPath )
hbs.registerPartials(viewPathPartials)

//SETUP STATIC DIRECTORY TO SERVE
app.use(express.static(publicDirPath))

//serving dynamic template
app.get('', (req,res) => {
    res.render('index', {
        title:'Weather App',
        fullname:"Chinwendu Onuoha"
    })
})

app.get('/about',(req, res) => {
    res.render('about', {
        title:'About Me :)',
        fullname:"Chinwendu Onuoha"
    })
})

app.get('/help',(req, res) => {
    res.render('help', {
       message:"Supposed to be a help dynamic page. But I'm still hard coding data.",
       title:'Help Page',
       fullname:"Chinwendu Onuoha"
    })
})

//low-level serving json
const address = process.argv[2]
app.get('/weather-info', (req,res) => {
    if(!req.query.address){
        return res.send('Provide an address for forecast')
    }
    geocode(req.query.address, (error, {location} = {}) => {
        if(error){
            return res.send({error})
        } 
        forecast(location, (error, forecastData) => {
            if(error) {
                return res.send(error)
            }
                res.send({
                    forecastInfo:forecastData,
                    location,
                    address:req.query.address
                })
            
           
        })
    })
    // console.log(req.query.address)
    // if(!req.query.address){
    //     return res.send('Input an address to get the weather forecast.')
    // }
    // res.send({
    //     "country" :"South Korea",
    //     "temperature": '16 degrees',
    //     "address":req.query.address
        
    // })
})

//higher-level json
app.get('/products', (req,res) => {
    if(!req.query.search){
        return res.send({
            error: 'You must provide a search term'
        })
    }
    console.log(req.query.search)
    res.send(
        {
            products:[]
        }
    )
})

app.get('/help/*', (req, res) => {
    res.render('pageNotFound', {
        message:"Help Article not",
        title:'404',
        fullname:"Chinwendu Onuoha"
     })
})

app.get('*', (req,res) => {
    res.render('404', {
        message:"Page not found",
        title:'404',
        fullname:"Chinwendu Onuoha"
     })
})

//app.com
//app.com/help
//app.com/about
//app.com/weather-info

app.listen(port, () =>{
    console.log('Server started on port ' + port)
})