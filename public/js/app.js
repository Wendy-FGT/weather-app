console.log('Client side js file is loaded!')

// fetch('https://puzzle.mead.io/puzzle').then((response => {
//     response.json().then((data) => {
//         console.log(data)
//     })
// }))



const weatherForm = document.querySelector('form')
const search = document.querySelector('input')
const lMessage = document.querySelector('#message-1')
const messageTwo = document.querySelector('#message-2')



weatherForm.addEventListener('submit', (e)=>{
    e.preventDefault()

    const location = search.value

    lMessage.textContent = 'Loading...'
    messageTwo.textContent= " "

    fetch('/weather-info?address=' + location).then((response) => {
    response.json().then((data) => {
        console.log(data.forecastInfo)
        if(data.error){
           lMessage.textContent = data.error
        } else {
            lMessage.textContent = data.location
            messageTwo.textContent = data.forecastInfo.forecast
        }
    })
})
})